//
//  QPopoverViewController.m
//  QPopoverController
//
//  Created by Changyuan on 2021/1/27.
//

#import "QPopoverViewController.h"


@interface QPopoverViewController ()

@property (nonatomic, strong) UIView * borderVirtualView; // 含箭头部分
@property (nonatomic, strong) UIView * borderContentView; // 不含箭头部分，用它来生成阴影
@property (nonatomic, strong) UIImageView * arrowView;

@end

@implementation QPopoverViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.modalPresentationStyle = UIModalPresentationCustom;
        [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        _dismissTouchInBackground = YES;
        _borderEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_userContentView == nil)
    {
        return;
    }
    
    if (_anchorRect.size.width < 0.01 && _anchorRect.size.height < 0.01)
    {
        _anchorRect = _anchorView.bounds;
    }
    
    BOOL isArrowUp = YES;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    
    CGSize contentSize = _userContentView.frame.size;
   
    CGFloat scale = _arrowImage.scale;
    if (scale < 0.001)
    {
        scale = 1.0;
    }
    
    CGFloat widthIcon = CGImageGetWidth(_arrowImage.CGImage) / scale;
    CGFloat heightIcon = CGImageGetHeight(_arrowImage.CGImage) / scale;
    
    CGRect rectArrow = CGRectZero;
    rectArrow.size = CGSizeMake(widthIcon, heightIcon);
    
    _userContentView.frame = CGRectMake(_borderEdgeInsets.left, _borderEdgeInsets.top, contentSize.width, contentSize.height);
    [self.borderContentView addSubview:_userContentView];
    
    CGRect rectBorder = CGRectMake(0, 0,
                                   contentSize.width + _borderEdgeInsets.left + _borderEdgeInsets.right,
                                   contentSize.height + _borderEdgeInsets.top + _borderEdgeInsets.bottom);
    
    CGRect rectVirtual = CGRectMake(0, 0,
                                    rectBorder.size.width + self.borderContentView.layer.shadowRadius * 2,
                                    rectBorder.size.height + self.borderContentView.layer.shadowRadius * 2);
    
    rectBorder.origin.x = (rectVirtual.size.width - rectBorder.size.width) / 2;
    rectBorder.origin.y = (rectVirtual.size.height - rectBorder.size.height) / 2;
    
    self.borderContentView.frame = rectBorder;
    
    _borderVirtualView = [[UIView alloc] init];
    
    [self.borderVirtualView addSubview:self.borderContentView];
    
    CGRect srect = [_anchorView convertRect:_anchorRect toView:self.view];
    if ((srect.origin.y + srect.size.height / 2) > (height / 2))
    {
        // 箭头向下
        isArrowUp = NO;
        rectVirtual.origin.y = srect.origin.y - rectVirtual.size.height + (self.borderContentView.layer.shadowRadius - heightIcon) - 1;
        rectArrow.origin.y = srect.origin.y - heightIcon - 1;
    }
    else
    {
        // 箭头向上
        isArrowUp = YES;
        rectVirtual.origin.y = srect.origin.y + srect.size.height - (self.borderContentView.layer.shadowRadius - heightIcon) + 1;
        rectArrow.origin.y = srect.origin.y + srect.size.height + 1;
    }
    
    rectArrow.origin.x = srect.origin.x + (srect.size.width - widthIcon) / 2;
    rectVirtual.origin.x = srect.origin.x + srect.size.width / 2 - rectVirtual.size.width / 2;
    if (rectVirtual.origin.x + rectVirtual.size.width > width)
    {
        rectVirtual.origin.x = width - rectVirtual.size.width;
    }
    
    if (rectVirtual.origin.x < 0)
    {
        rectVirtual.origin.x = 0;
    }
    
    UIImage * image = _arrowImage;
    if (!isArrowUp)
    {
        image = [UIImage imageWithCGImage:_arrowImage.CGImage scale:1.0 orientation:UIImageOrientationDownMirrored];
    }
    
    _arrowView = [[UIImageView alloc] initWithFrame:rectArrow];
    //_arrowView.layer.borderWidth = 1;
    //_arrowView.layer.borderColor = [UIColor redColor].CGColor;
    [_arrowView setImage:image];
    
    _borderVirtualView.frame = rectVirtual;
    [self.view addSubview:_borderVirtualView];
    [self.view addSubview:_arrowView];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)eventv
{
    if (_dismissTouchInBackground)
    {
        CGPoint touchPoint = [[touches anyObject] locationInView:self.borderContentView];
        CGPoint touchPointArraw = [[touches anyObject] locationInView:self.arrowView];
        
        if (![self.borderContentView pointInside:touchPoint withEvent:eventv] && ![self.arrowView pointInside:touchPointArraw withEvent:eventv])
         {
            [self dismissViewControllerAnimated:YES completion:nil];
         }
    }
}

- (UIView *)borderContentView
{
    if (_borderContentView == nil)
    {
        _borderContentView = [[UIView alloc] init];
        _borderContentView.backgroundColor = [UIColor whiteColor];
        _borderContentView.layer.cornerRadius = 8;
        _borderContentView.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.3].CGColor;
        _borderContentView.layer.shadowOffset = CGSizeMake(0, 2);
        _borderContentView.layer.shadowOpacity = 0.8;//阴影透明度，默认0
        _borderContentView.layer.shadowRadius = 30;
    }
    
    return _borderContentView;
}

@end
