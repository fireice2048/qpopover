//
//  ViewController.m
//  QPopoverController
//
//  Created by Changyuan on 2021/1/27.
//

#import "ViewController.h"
#import "QPopoverViewController.h"

@interface ViewController ()

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    UIButton * btn1 = [[UIButton alloc] initWithFrame:CGRectMake(40, 180, 100, 30)];
    btn1.backgroundColor = [UIColor greenColor];
    [btn1 addTarget:self action:@selector(btn1Click:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:btn1];
    
    UIButton * btn2 = [[UIButton alloc] initWithFrame:CGRectMake(240, 560, 100, 30)];
    btn2.backgroundColor = [UIColor blueColor];
    [btn2 addTarget:self action:@selector(btn2Click:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:btn2];
    
    UIButton * btn3 = [[UIButton alloc] initWithFrame:CGRectMake(40 + 200, 180, 100, 30)];
    btn3.backgroundColor = [UIColor greenColor];
    [btn3 addTarget:self action:@selector(btn3Click:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:btn3];
}

- (void)btn1Click:(UIView *)sender
{
    UIView * view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 120)];
    view1.backgroundColor = [UIColor brownColor];
    
    QPopoverViewController * popover = [[QPopoverViewController alloc] init];
    popover.userContentView = view1;
    popover.anchorView = sender;
    
    UIImage * image = [UIImage imageNamed:@"popover_arrow_icon"];
    popover.arrowImage = image;
    
    [self presentViewController:popover animated:YES completion:nil];
}

- (void)btn2Click:(UIView *)sender
{
    UIView * view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 120)];
    view2.backgroundColor = [UIColor orangeColor];
    
    QPopoverViewController * popover = [[QPopoverViewController alloc] init];
    popover.userContentView = view2;
    popover.anchorView = sender;

    CGRect anchorRect = sender.bounds;
    anchorRect.origin.y -= 10;
    anchorRect.size.height += 20;
    popover.anchorRect = anchorRect;
    
    UIImage * image = [UIImage imageNamed:@"popover_arrow_icon"];
    image = [self scaleToSize:CGSizeMake(36, 22) image:image];
    popover.arrowImage = image;
    
    //popover.borderContentView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.4];
    
    [self presentViewController:popover animated:YES completion:nil];
}

- (void)btn3Click:(UIView *)sender
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 120)];
    
    QPopoverViewController * popover = [[QPopoverViewController alloc] init];
    popover.userContentView = view;
    popover.anchorView = sender;
    
    //popover.borderContentView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.4];
    
    CGRect anchorRect = sender.bounds;
    //anchorRect.origin.y -= 10;
    //anchorRect.size.height += 20;
    popover.anchorRect = anchorRect;
    
    UIImage * image = [UIImage imageNamed:@"popover_arrow_icon"];
    image = [self scaleToSize:CGSizeMake(36, 22) image:image];
    popover.arrowImage = image;
    
    [self presentViewController:popover animated:YES completion:nil];
}

- (UIImage *)scaleToSize:(CGSize)size image:(UIImage *)image
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
    
    return scaledImage;
}

@end

