//
//  QPopoverViewController.h
//  QPopoverController
//
// 支持圆角阴影、箭头只支持上下方向
//
//  Created by Changyuan on 2021/1/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface QPopoverViewController : UIViewController

@property (nonatomic, strong) UIImage * arrowImage; // 箭头向上图像
@property (nonatomic, strong) UIView * anchorView; // 锚点View
@property (nonatomic, assign) CGRect anchorRect; // 锚点Rect，默认为anchorView.bounds
@property (nonatomic, assign) BOOL dismissTouchInBackground; // 背景点击是否关闭弹窗，默认为YES
@property (nonatomic, strong) UIView * userContentView; // 外部传入的内容视图
@property (nonatomic, assign) UIEdgeInsets borderEdgeInsets; // 圆形边框四周border大小，默认为10,10,10,10

@property (nonatomic, readonly) UIView * borderContentView; // 不含箭头部分，用它来生成阴影，可以访问其layer修改阴影参数

@end

NS_ASSUME_NONNULL_END

