# QPopover

#### 介绍
QPopover, iOS圆角阴影气泡控件，支持圆角阴影、箭头只支持上下方向，只需提供一张箭头向上的箭头图片

#### 示例效果
![输入图片说明](https://gitee.com/fireice2048/qpopover/raw/master/Example/demo.png "示例效果")

#### 安装教程

1.  首先下载QPopover项目源码
2.  将 QPopoverViewController.h / QPopoverViewController.m 复制到目标项目中
3.  再将QPopover项目中 popover_arrow_icon@3x.png 图片添加到目标项目中
4.  然后按以下使用说明编写代码即可


#### 使用说明


```
#import "QPopoverViewController.h"

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(40, 180, 100, 30)];
    button.backgroundColor = [UIColor greenColor];
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:button];
}

- (void)buttonClick:(UIView *)sender
{
    // 创建自定义ContentView，放置自己的业务UI元素，正确设置宽度和高度（注意预留阴影大小，默认左右均为30像素），Popover会自适应其大小
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 120)];
    view.backgroundColor = [UIColor orangeColor];
    
    QPopoverViewController * popover = [[QPopoverViewController alloc] init];
    popover.userContentView = view;
    popover.anchorView = sender; // 设置锚点视图，尖角箭头将居中对准该视图，可以通过 borderEdgeInsets 进行微调
    
    UIImage * image = [UIImage imageNamed:@"popover_arrow_icon"];
    popover.arrowImage = image; // 设置尖角箭头图片
    
    [self presentViewController:popover animated:YES completion:nil];
}

```

1.  可以修改阴影的参数：

```
    popover.borderContentView.layer.cornerRadius = 8;
    popover.borderContentView.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.5].CGColor;
    popover.borderContentView.layer.shadowOffset = CGSizeMake(0, 3);
    popover.borderContentView.layer.shadowOpacity = 0.8;//阴影透明度，默认0
    popover.borderContentView.layer.shadowRadius = 20;

```

2.  也可以修改圆角边框视图的背景颜色

```
popover.borderContentView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.4];
```
当修改了圆角边框背景颜色时，应该将尖角箭头图片替换为相适应的图片

```
    popover.arrowImage = [UIImage imageNamed:@"xxx"];;
```


3.  可以修改圆角边框的四周Border大小
```
popover.borderEdgeInsets = UIEdgeInsetsMake(16, 16, 16, 16);
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
